import React from 'react';
import { shallow } from 'enzyme';
import Series from './index';
import { API_URL } from '../../helpers/constants';

describe('the <Series /> page', () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it('should request data from the API', () => {
    fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));
    const fetchMovies = () => new Promise(() => {});

    fetchMovies(API_URL).then((res) => {
      expect(res.data).toEqual('12345');
    });
  });

  it('should render loading when data fetching has started', () => {
    fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));
    const fetchMovies = () => new Promise(() => {});
    const wrapper = shallow(<Series />);

    fetchMovies(API_URL).then((res) => {
      expect(wrapper.text()).to.equal('Loading...');
    });
  });

  it('should render an error when data fetching has failed', () => {
    fetch.mockResponseOnce(JSON.stringify({ data: '12345' }));
    const fetchMovies = fetch.mockReject((res) => Promise.reject(res.errorToRaise));
    const wrapper = shallow(<Series />);

    fetchMovies(API_URL).catch(() => {
      expect(wrapper.text().to.equal('Oops, something went wrong...'));
    });
  });
});
