import React, { useEffect, useState, Fragment } from 'react';
import { StyledContainer } from '../../helpers/styles/grid';
import { API_URL, MEDIA_CATEGORIES, LIMIT } from '../../helpers/constants';
import { formatData } from '../../helpers/utils/utils';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import Footer from '../../components/Footer';
import Loading from '../../components/Loading';
import MediaList from '../../components/MediaList';
import Error from '../../components/Error';

const Series = () => {
  const [media, setMedia] = useState();
  const [error, setError] = useState();
  const [loading, setLoading] = useState();

  useEffect(() => {
    fetchMedia();
  }, []);

  const fetchMedia = () => {
    setLoading(true);
    fetch(API_URL)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setLoading(false);
        setMedia(formatData(data.entries, LIMIT, MEDIA_CATEGORIES.SERIES));
      })
      .catch(() => {
        setLoading(false);
        setError(true);
      });
  };

  return (
    <Fragment>
      <Header />
      <Banner title="Popular Series" />
      <StyledContainer>
        {loading && <Loading />}
        {media && <MediaList media={media} />}
        {error && <Error />}
      </StyledContainer>
      <Footer fixed />
    </Fragment>
  );
};

export default Series;
