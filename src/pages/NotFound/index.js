import React, { Fragment } from 'react';
import Error from '../../components/Error';
import Header from '../../components/Header';
import { StyledContainer } from '../../helpers/styles/grid';
import { Link } from 'react-router-dom';

const NotFound = () => (
  <Fragment>
    <Header />
    <StyledContainer>
      <Error />
      <Link to="/">Return Home</Link>
    </StyledContainer>
  </Fragment>
);

export default NotFound;
