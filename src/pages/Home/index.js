import React, { Fragment } from 'react';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import Footer from '../../components/Footer';
import MediaCard from '../../components/MediaCard';
import placeholderImage from '../../assets/placeholder.png';
import { StyledContainer, StyledRowFluid } from '../../helpers/styles/grid';

const Home = () => {
  const categories = [
    {
      name: 'Series',
      title: 'Popular Series',
      url: '/series',
      imageUrl: placeholderImage,
    },
    {
      name: 'Movies',
      title: 'Popular Movies',
      url: '/movies',
      imageUrl: placeholderImage,
    },
  ];

  const renderCategoryCard = () =>
    categories.map((category) => (
      <MediaCard
        key={category.title}
        title={category.title}
        name={category.name}
        imageUrl={category.imageUrl}
        type="default"
        url={category.url}
      />
    ));

  return (
    <Fragment>
      <Header />
      <Banner title="Popular Titles" />
      <StyledContainer>
        <StyledRowFluid>{renderCategoryCard()}</StyledRowFluid>
      </StyledContainer>
      <Footer />
    </Fragment>
  );
};

export default Home;
