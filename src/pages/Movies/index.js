import React, { useEffect, useState, Fragment } from 'react';
import { API_URL, MEDIA_CATEGORIES, LIMIT } from '../../helpers/constants';
import { formatData } from '../../helpers/utils/utils';
import { StyledContainer } from '../../helpers/styles/grid';
import Header from '../../components/Header';
import Banner from '../../components/Banner';
import Loading from '../../components/Loading';
import Error from '../../components/Error';
import MediaList from '../../components/MediaList';
import Footer from '../../components/Footer';

const Movies = () => {
  const [movies, setMovies] = useState();
  const [error, setError] = useState();
  const [loading, setLoading] = useState();

  useEffect(() => {
    fetchMovies();
  }, []);

  const fetchMovies = () => {
    setLoading(true);
    fetch(API_URL)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setLoading(false);
        setMovies(formatData(data.entries, LIMIT, MEDIA_CATEGORIES.MOVIE));
      })
      .catch(() => {
        setLoading(false);
        setError(true);
      });
  };

  return (
    <Fragment>
      <Header />
      <Banner title="Popular Movies" />
      <StyledContainer>
        {loading && <Loading />}
        {movies && <MediaList media={movies} />}
        {error && <Error />}
      </StyledContainer>
      <Footer fixed />
    </Fragment>
  );
};

export default Movies;
