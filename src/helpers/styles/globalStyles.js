import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html, 
  body{ 
    height: 100%; 
    margin:0; 
  }

  body{
    font-family: 'Raleway', sans-serif;
    font-size: 1.5rem;
    line-height: 2;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: 0;
  }


  h3,h4,h5 {
    font-weight: 600;
  }

  li {
    font-size: 16px;
  }

  p, a, span { 
    font-family: 'Raleway', sans-serif;
    font-size: 16px;
    margin: 0;
    line-height: 26px;
    font-weight: 400;
  }
`;
