import styled from 'styled-components';

export const StyledContainer = styled.div`
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;

  @media (min-width: 992px) {
    width: 970px;
  }
  @media (min-width: 1200px) {
    width: 1170px;
  }
`;

export const StyledRow = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
  padding: 1.5rem 0;
  margin: 0 -10px;
`;

export const StyledRowFluid = styled(StyledRow)`
  margin: 0;
`;

export const StyledCol = styled.div`
  flex-basis: 14%;
  width: 200px;
  padding: 10px;
  position: relative;
  box-sizing: border-box;
  margin: 0 auto;
  cursor: pointer;
  line-height: 0;

  @media (max-width: 1333px) {
    flex-basis: 20%;
  }

  @media (max-width: 1073px) {
    flex-basis: 20%;
  }

  @media (max-width: 815px) {
    flex-basis: 50%;
  }
`;
