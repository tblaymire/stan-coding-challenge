import { formatData, sortAlphabetically } from './utils';
import { movieData, mixedMediaData } from './test-helper';

describe('the sortAlphabetically helper function', () => {
  it('should sort the media array alphabetically', () => {
    const expectedOrder = [
      { title: 'American Sniper', programType: 'movie', releaseYear: 2000 },
      { title: 'John Wick', programType: 'movie', releaseYear: 2010 },
      { title: 'San Andreas', programType: 'movie', releaseYear: 2020 },
    ];

    const mediaArray = movieData.sort(sortAlphabetically);
    expect(mediaArray).toEqual(expectedOrder);
  });
});

describe('the formatData helper function', () => {
  it('should sort the media array by release year', () => {
    const LIMIT = 3;
    const PROGRAM_TYPE = 'movie';

    const expectedOrder = [
      {
        title: 'American Sniper',
        programType: 'movie',
        releaseYear: 2010,
      },
      {
        title: 'The Wolf Of Wall Street',
        programType: 'movie',
        releaseYear: 2010,
      },
    ];

    const mediaArray = formatData(mixedMediaData, LIMIT, PROGRAM_TYPE);
    expect(mediaArray).toEqual(expectedOrder);
  });

  it('should return media by matching program type of "movie"', () => {
    const LIMIT = 1;
    const PROGRAM_TYPE = 'movie';

    const expectedOrder = [
      {
        title: 'The Wolf Of Wall Street',
        programType: 'movie',
        releaseYear: 2010,
      },
    ];

    const mediaArray = formatData(mixedMediaData, LIMIT, PROGRAM_TYPE);
    expect(mediaArray).toEqual(expectedOrder);
  });

  it('should return media by matching program type of "series"', () => {
    const LIMIT = 1;
    const PROGRAM_TYPE = 'series';
    const expectedOrder = [
      {
        title: 'Family Guy',
        programType: 'series',
        releaseYear: 2015,
      },
    ];

    const mediaArray = formatData(mixedMediaData, LIMIT, PROGRAM_TYPE);
    expect(mediaArray).toEqual(expectedOrder);
  });

  it('should return the correct media ammount of items by limit constant', () => {
    const LIMIT = 2;
    const PROGRAM_TYPE = 'movie';
    const mediaArray = formatData(mixedMediaData, LIMIT, PROGRAM_TYPE);
    expect(mediaArray.length).toEqual(2);
  });
});
