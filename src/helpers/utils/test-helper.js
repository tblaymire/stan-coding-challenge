export const movieData = [
  {
    title: 'John Wick',
    programType: 'movie',
    releaseYear: 2010,
  },
  {
    title: 'American Sniper',
    programType: 'movie',
    releaseYear: 2000,
  },
  {
    title: 'San Andreas',
    programType: 'movie',
    releaseYear: 2020,
  },
];

export const mixedMediaData = [
  {
    title: 'The Wolf Of Wall Street',
    programType: 'movie',
    releaseYear: 2010,
  },
  {
    title: 'Family Guy',
    programType: 'series',
    releaseYear: 2015,
  },
  {
    title: 'American Sniper',
    programType: 'movie',
    releaseYear: 2010,
  },
  {
    title: 'The Green Mile',
    programType: 'movie',
    releaseYear: 1999,
  },
];
