export const formatData = (data, limit, programType) => {
  const filtered = data.filter((item) => item.releaseYear >= 2010 && item.programType === programType).slice(0, limit);
  return filtered.sort(sortAlphabetically).slice(0, limit);
};

export const sortAlphabetically = (x, y) => {
  if (x.title < y.title) return -1;
  if (x.title > y.title) return 1;
  return 0;
};
