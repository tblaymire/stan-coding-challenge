export const API_URL = 'https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json';

export const LIMIT = 21;

export const MEDIA_CATEGORIES = {
  MOVIE: 'movie',
  SERIES: 'series',
};
