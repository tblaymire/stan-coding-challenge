import React, { Fragment, Suspense } from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { GlobalStyle } from './helpers/styles/globalStyles';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
  <Fragment>
    <GlobalStyle />
    <BrowserRouter>
      <Suspense fallback={''}>
        <App />
      </Suspense>
    </BrowserRouter>
  </Fragment>,
  document.getElementById('root'),
);
