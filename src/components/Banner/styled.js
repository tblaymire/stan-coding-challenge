import styled from 'styled-components';

export const StyledBanner = styled.div`
  background: #414141;
  color: #fff;
  font-size: 1.3rem;
  line-height: 24px;
  padding: 0.5rem 0;
`;
