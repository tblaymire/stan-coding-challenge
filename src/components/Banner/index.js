import React from 'react';
import { StyledBanner } from './styled';
import { StyledContainer } from '../../helpers/styles/grid';

const Banner = ({ title }) => (
  <StyledBanner>
    <StyledContainer>
      <span>{title}</span>
    </StyledContainer>
  </StyledBanner>
);

export default Banner;
