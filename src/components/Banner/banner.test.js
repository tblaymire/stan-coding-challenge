import React from 'react';
import { shallow } from 'enzyme';
import Banner from './index';
import { StyledBanner } from './styled';

describe('the <Banner /> component', () => {
  const cardProps = { title: '' };

  it('should render a banner title', () => {
    const wrapper = shallow(<Banner {...cardProps} title="Popular Titles" />);
    expect(wrapper.find(StyledBanner).text()).toBe('Popular Titles');
  });
});
