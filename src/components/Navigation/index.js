import React from 'react';
import { StyledNavigation, StyledNavLink } from './styled';
import Button from '../../components/Button';

const Navigation = () => (
  <StyledNavigation>
    <StyledNavLink to="/login">Log in</StyledNavLink>
    <Button>Start your free trial</Button>
  </StyledNavigation>
);

export default Navigation;
