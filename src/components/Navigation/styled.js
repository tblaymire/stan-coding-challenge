import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const StyledNavigation = styled.div`
  display: flex;
  align-items: center;

  button {
    display: none;
    @media (min-width: 480px) {
      display: initial;
    }
  }
`;

export const StyledNavLink = styled(Link)`
  text-decoration: none;
  color: #fff;
  font-size: 0.9rem;

  @media (min-width: 480px) {
    padding-right: 1.5rem;
  }
`;
