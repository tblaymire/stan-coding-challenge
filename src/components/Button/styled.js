import styled from 'styled-components';

export const StyledButton = styled.button`
  padding: 0.5rem 1rem;
  background: #414141;
  color: white;
  border: none;
  font-size: 0.9rem;
  cursor: pointer;
`;
