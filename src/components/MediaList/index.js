import React from 'react';
import MediaCard from '../MediaCard';
import { StyledRow, StyledCol } from '../../helpers/styles/grid';

const MediaList = ({ media }) => {
  const renderMediaItems = () =>
    media.map((mediaItem) => (
      <StyledCol key={mediaItem.title}>
        <MediaCard title={mediaItem.title} imageUrl={mediaItem.images['Poster Art'].url} />
      </StyledCol>
    ));
  return <StyledRow>{renderMediaItems()}</StyledRow>;
};

export default MediaList;
