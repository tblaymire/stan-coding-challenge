import React from 'react';

const Error = () => <div>Oops, something went wrong...</div>;

export default Error;
