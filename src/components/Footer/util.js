import facebookIcon from '../../assets/social/facebook-white.svg';
import twitterIcon from '../../assets/social/twitter-white.svg';
import instagramIcon from '../../assets/social/instagram-white.svg';
import appStore from '../../assets/store/app-store.svg';
import playStore from '../../assets/store/play-store.svg';

export const footerMenuItems = [
  {
    name: 'Home',
    link: '/home',
  },
  {
    name: 'Terms and Conditions',
    link: '/terms-and-conditions',
  },
  {
    name: 'Privacy Policy',
    link: '/privacy-policy',
  },
  {
    name: 'Collection Statement',
    link: '/collection-statement',
  },
  {
    name: 'Help',
    help: '/help',
  },
  {
    name: 'Manage Account',
    link: '/manage-account',
  },
];

export const socialMenuItems = [
  {
    name: 'Facebook',
    icon: facebookIcon,
  },
  {
    name: 'Twitter',
    icon: twitterIcon,
  },
  {
    name: 'Instagram',
    icon: instagramIcon,
  },
];

export const appIcons = [
  {
    name: 'App Store',
    icon: appStore,
  },
  {
    name: 'Play Store',
    icon: playStore,
  },
];
