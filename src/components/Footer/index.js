import React from 'react';
import {
  StyledFooter,
  StyledFooterList,
  StyledFooterItem,
  StyledFooterCopyright,
  StyledFooterLower,
  StyledFooterSocialIcon,
  StyledFooterAppIcon,
} from './styled';
import { StyledContainer } from '../../helpers/styles/grid';
import { footerMenuItems, socialMenuItems, appIcons } from './util';

const Footer = ({ fixed }) => {
  const renderFooterMenu = () =>
    footerMenuItems.map((item) => <StyledFooterItem key={item.name}>{item.name}</StyledFooterItem>);

  const renderSocialIcons = () =>
    socialMenuItems.map((item) => <StyledFooterSocialIcon key={item.name} src={item.icon} alt={item.name} />);

  const renderAppIcons = () =>
    appIcons.map((item) => <StyledFooterAppIcon key={item.name} src={item.icon} alt={item.name} />);

  return (
    <StyledFooter fixed={fixed}>
      <StyledContainer>
        <StyledFooterList>{renderFooterMenu()}</StyledFooterList>
        <StyledFooterCopyright>Copyright © 2016 DEMO Straming All Rights Reserved.</StyledFooterCopyright>
        <StyledFooterLower>
          <div>{renderSocialIcons()}</div>
          <div>{renderAppIcons()}</div>
        </StyledFooterLower>
      </StyledContainer>
    </StyledFooter>
  );
};

export default Footer;
