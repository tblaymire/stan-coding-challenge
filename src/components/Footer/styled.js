import React from 'react';
import styled from 'styled-components';

export const StyledFooter = styled(({ fixed, ...props }) => <footer {...props} />)`
  background: #1e1e1e;
  padding: 2rem 0;
  margin-top: auto;
  position: absolute;
  position: ${({ fixed }) => (fixed ? 'relative' : 'absolute')};
  bottom: 0;
  width: 100%;
`;

export const StyledFooterList = styled.ul`
  color: #8f938f;
  list-style: none;
  padding: 0;
  margin: 0;
  line-height: 24px;

  @media (min-width: 480px) {
    display: flex;
  }
`;

export const StyledFooterItem = styled.li`
  cursor: pointer;
  font-size: 0.9rem;
  padding-bottom: 0.5rem;
  transition: all 0.4s;

  &:hover {
    opacity: 0.5;
  }

  &:not(:last-child) {
    padding-right: 1rem;
  }

  @media (min-width: 480px) {
    &:not(:last-child):after {
      margin-left: 10px;
      padding-bottom: 0;
      content: '|';
    }
  }
`;

export const StyledFooterSocialIcon = styled.img`
  width: 30px;
  height: 30px;
  cursor: pointer;
  &:not(:last-child) {
    padding-right: 1rem;
  }
`;

export const StyledFooterCopyright = styled.span`
  color: #7b7d82;
  font-size: 0.8rem;
`;

export const StyledFooterLower = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-top: 1rem;

  @media (min-width: 480px) {
    flex-direction: row;
  }
`;

export const StyledFooterAppIcon = styled.img`
  &:first-child {
    padding-right: 1rem;
  }
`;
