import React from 'react';
import { StyledLogo } from './styled';

const Logo = () => <StyledLogo to="/">DEMO Streaming</StyledLogo>;

export default Logo;
