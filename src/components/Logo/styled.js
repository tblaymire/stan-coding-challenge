import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const StyledLogo = styled(Link)`
  text-decoration: none;
  font-weight: 600;
  font-size: 1.5rem;
  color: #fff;

  @media (min-width: 480px) {
    font-size: 1.9rem;
  }
`;
