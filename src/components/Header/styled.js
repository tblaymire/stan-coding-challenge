import styled from 'styled-components';

export const StyledHeader = styled.header`
  background: #037dfe;
  position: relative;
  box-shadow: 0 6px 6px -6px black;
  padding: 1rem 0;
`;

export const StyledWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
