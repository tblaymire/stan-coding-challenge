import React from 'react';
import Logo from '../Logo';
import Navigation from '../Navigation';
import { StyledHeader, StyledWrapper } from './styled';
import { StyledContainer } from '../../helpers/styles/grid';

const Header = () => (
  <StyledHeader>
    <StyledContainer>
      <StyledWrapper>
        <Logo />
        <Navigation />
      </StyledWrapper>
    </StyledContainer>
  </StyledHeader>
);

export default Header;
