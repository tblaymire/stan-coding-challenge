import React from 'react';
import {
  StyledMediaCard,
  StyledMediaTitle,
  StyledMediaImage,
  StyledMediaLink,
  StyledDefaultName,
  StyledDefaultCard,
  StyledDefaultContent,
  StyledDefaultPageTitle,
} from './styled';

const MediaCard = ({ title, name, imageUrl, type, url }) => {
  const renderMediaCard = () => (
    <StyledMediaCard type={type}>
      {imageUrl && <StyledMediaImage src={imageUrl} alt={title} />}
      <StyledMediaTitle>{title}</StyledMediaTitle>
    </StyledMediaCard>
  );

  const renderDefaultCard = () => (
    <StyledDefaultCard>
      <StyledDefaultContent>
        <StyledDefaultName>{name}</StyledDefaultName>
        <StyledMediaImage src={imageUrl} alt={title} />
      </StyledDefaultContent>
      <StyledDefaultPageTitle>{title}</StyledDefaultPageTitle>
    </StyledDefaultCard>
  );

  return type === 'default' ? <StyledMediaLink to={url}>{renderDefaultCard()}</StyledMediaLink> : renderMediaCard();
};

export default MediaCard;
