import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const StyledMediaCard = styled.div`
  line-height: 1em;
`;

export const StyledMediaImage = styled.img`
  width: 100%;
`;

export const StyledMediaTitle = styled.span`
  font-size: 1rem;
  line-height: 1.4em;
`;

export const StyledMediaLink = styled(Link)`
  text-decoration: none;
`;

export const StyledDefaultCard = styled.div`
  &:first-child {
    margin-right: 1rem;
  }
`;

export const StyledDefaultContent = styled.div`
  width: 145px;
  height: 220px;
  background: #1e1e1e;
  color: #fff;
  text-transform: uppercase;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;

export const StyledDefaultName = styled.span`
  position: absolute;
  font-weight: 600;
  font-size: 1.8rem;
`;

export const StyledDefaultPageTitle = styled(StyledMediaTitle)`
  color: #1e1e1e;
`;

StyledMediaTitle.displayName = 'MediaTitle';
StyledDefaultName.displayName = 'DefaultName';
