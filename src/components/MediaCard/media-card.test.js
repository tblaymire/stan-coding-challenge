import React from 'react';
import { shallow } from 'enzyme';
import MediaCard from './index';
import { StyledMediaImage } from './styled';

describe('the <MediaCard /> component', () => {
  const cardProps = {
    title: '',
    imageUrl: '',
    url: '',
  };

  it('should render a media card with an image', () => {
    const wrapper = shallow(<MediaCard {...cardProps} imageUrl="card.jpg" />);
    expect(wrapper.find(StyledMediaImage));
    expect(wrapper.find(StyledMediaImage).prop('src')).toBe('card.jpg');
  });

  it('should render a media card with a title', () => {
    const wrapper = shallow(<MediaCard {...cardProps} title="American Sniper" />);
    expect(wrapper.find('MediaTitle').text()).toBe('American Sniper');
  });
});
