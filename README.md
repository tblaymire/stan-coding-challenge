## Run Instructions

In the project directory, you can run: `yarn start` to run the project and `yarn test` to run the unit tests.

## Live URL

https://quizzical-rosalind-4a70fc.netlify.app/

[![Netlify Status](https://api.netlify.com/api/v1/badges/a8a9a037-b6c2-4f3d-953f-57d7161e5b01/deploy-status)](https://app.netlify.com/sites/quizzical-rosalind-4a70fc/deploys)

## Developer Considerations

In terms of app setup I tried to keep things as simple as possible. I decided to implement multiple renderX functions within components
to help keep the JSX as readable as possible, this should also help in the future as the UI evolves.

I did decide to use Styled Components, apologies if this is a problem. It allowed for a much more rapid development and can be extended in the future.
Happy to discuss my thoughts and reasoning around this and help with any questions.

The formatData chain usage of `.filter()` was a consideration but I believe this will have minor performance improvements over seperate functions. This
could eaisly be seperated in the future.

## Future Optimizations

**API Helper**

Given more time I would create an API helper to fix the duplication between the Movies & Series page and keep things more DRY. They are essentially the
same functionality.

**Helper Styles**

I would also implement helpers for styles and theme variables (fonts, spacing, weights, etc).
The spacing would be especially useful to adhear to a standard e.g `${multiply(spacer, 1.5)}`;
with spacer been set to the base 16px to keep things consistant. (Happy to explain this in detail.)

**Lazy Loading**

I have implemented a base Lazy Load by Route, the performance is questionable here but I think if we were rendering more movies with
some form of pagination this would help a lot with a fallback (Ghosted) UI too.

**Type Checking**

It would be good to explore using either the old-school PropTypes or Typescript in the future to ensure that the correct value types are passed the Intellisense would help a lot too!

**Unit Tests**

Given more time I would look to cover more unit test scenarios especially one for the Home page.

**Mobile Gotchas**

I made a few assumptions in terms of mobile layout and sizing, the mobile menu would be an important one I would question with design as I'm just hiding the
CTA Button at the minute. I would also extend the Button component and aim to make this more flexible with onClick and Link functionaility.

**Page Speed Insights**

This could 100% be improved, minor things such as time to interactive but accessiblity was also identified with the color choices.

## Feedback

Overall a great challenge, I really enjoyed it! 👍
